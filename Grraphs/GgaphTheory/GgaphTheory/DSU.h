//
//  DSU.h
//  Graphs
//
//  Created by Nikita Babonov on 22/05/17.
//  Copyright © 2017 SUSU. All rights reserved.
//

#ifndef DSU_h
#define DSU_h
#include <fstream>
#include <map>
#include <istream>
#include <ostream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <stack>
using std::vector;

class DSU {
private: vector <int> p; vector <int> value;
public:
    int find(int x) {
        return p[x] == x ? x : p[x] = find(p[x]);
    }
    void unite(int x, int y) {
        x = find(x); y = find(y);
        if (value[x] < value[y]) p[x] = y;
        else {
            p[y] = x;
            if (value[x] == value[y]) value[x]++;
        }
    }
    DSU(int n) {
        p.resize(n + 1);
        value.resize(n + 1);
        for (int i = 1; i <= n; i++) {
            p[i] = i;
            value[i] = 1;
        }
    }
    bool check() {
        for (int i = 1; i < p.size() - 1; i++) if (find(p[i]) != find(p[i + 1])) return false;
        return true;
    }
};



#endif /* DSU_h */
