//
//  DSU.h
//  Graphs
//
//  Created by Nikita Babonov on 22/05/17.
//  Copyright © 2017 SUSU. All rights reserved.
//

#ifndef Graph_h
#define Graph_h

using namespace std;

class Graph{
    char  statement; int numOfEdges;
    int countOfList; int weighted;
    int isOriented;
    vector<vector<int>> adjacencyMatrix;
    vector<map<int, int>> adjacencyList;
    vector<tuple<int, int, int>>listOfEdges;

void setGraph(char state, int oriented, int weighed, int count, int edges){
        statement = state; isOriented = oriented;
        weighted = weighed; countOfList = count;
        numOfEdges = edges;
        switch (state){
            case ADJLIST: adjacencyList.resize(countOfList); break;
            case ADJMATRIX:{
                adjacencyMatrix.resize(countOfList);
                for (int i = 0; i < countOfList; i++)
                    adjacencyMatrix[i].resize(countOfList);
           }
                break;
            case LISTOFEDGES: listOfEdges.resize(numOfEdges); break;
       }
}
    
Graph(){}
    
void  readGraph(string inputFileName){
        freopen(inputFileName.c_str(), "r", stdin);
        cin >>  statement;
        switch ( statement){
            case ADJMATRIX:{
                cin >> countOfList >> isOriented >> weighted;
                adjacencyMatrix.resize(countOfList);
                for (int i = 0; i < countOfList; i++) adjacencyMatrix[i].resize(countOfList);
                for (int i = 0; i < countOfList; i++){
                    for (int j = 0; j < countOfList; j++){
                        int edge;
                        cin >> edge;
                        if (edge) numOfEdges++;
                        adjacencyMatrix[i][j] = edge;
                        if (!isOriented) adjacencyMatrix[j][i] = edge;
                   }
               }
                if (!isOriented) numOfEdges /= 2; break;
           }
            case ADJLIST:{
                cin >> countOfList >> isOriented >> weighted;
                adjacencyList.resize(countOfList);
                string singleString;
                for (int i = 0; i < countOfList; i++){
                    getline(cin, singleString);
                    istringstream stringStream(singleString);
                    int verticle, weight = 1;
                    while (stringStream){
                        stringStream >> verticle;
                        if (weighted){ stringStream >> weight;}
                        adjacencyList[i][verticle] = weight;
                   }
                    numOfEdges += adjacencyList[i].size();
               }
                break;
           }
            case LISTOFEDGES:{
                cin >> countOfList >> numOfEdges >> isOriented >> weighted;
                listOfEdges.resize(numOfEdges);
                int from, to, weight = 1;
                for (int i = 0; i < numOfEdges; i++){
                    cin >> from >> to;
                    if (weighted) cin >> weight;
                    get<FROM_INDEX>(listOfEdges[i]) = from;
                    get<TO_INDEX>(listOfEdges[i]) = to;
                    get<WEIGHT_INDEX>(listOfEdges[i]) = weight;
               }
                break;
           }
       }
}

void  writeGraph(string outputFileName){
        freopen(outputFileName.c_str(), "w", stdout);
        switch ( statement){
            case ADJMATRIX:{
                cout <<  statement << " " << countOfList << endl;
                cout << isOriented << " " << weighted << endl;
                for (int i = 0; i < countOfList; i++){
                    for (int j = 0; j < countOfList; j++) cout << adjacencyMatrix[i][j] << " ";
                    cout << endl;
               }
                break;
           }
            case ADJLIST:{
                cout <<  statement << " " << countOfList << endl;
                cout << isOriented << " " << weighted << endl;
                for (int i = 0; i < countOfList; i++){
                    for (auto it = adjacencyList[i].begin(); it != adjacencyList[i].end(); it++){
                        cout << it->first << ' ';
                        if (weighted) cout << it->second;
                   }
                    cout << endl;
               }
                break;
           }
            case LISTOFEDGES:{
                cout <<  statement << " " << countOfList << " " << numOfEdges << endl;
                cout << isOriented << " " << weighted << endl;
                for (int i = 0; i < numOfEdges; i++){
                    cout << get<FROM_INDEX>(listOfEdges[i]) << " " << get<TO_INDEX>(listOfEdges[i]) << " ";
                    if (weighted) cout << get<WEIGHT_INDEX>(listOfEdges[i]);
                    cout << endl;
               }
                break;
           }
       }
}

void  addEdge(int from, int to, int weight = 1){
        switch ( statement){
            case ADJMATRIX:{
                adjacencyMatrix[from-1][to-1] = weight;
                if (!isOriented) adjacencyMatrix[to-1][from-1] = weight;
                numOfEdges++;
                break;
           }
            case ADJLIST:{
                adjacencyList[from-1][to] = weight;
                if (!isOriented) adjacencyList[to-1][from] = weight;
                numOfEdges++;
                break;
           }
            case LISTOFEDGES:{
                numOfEdges++;
                listOfEdges.resize(numOfEdges);
                get<FROM_INDEX>(listOfEdges[numOfEdges-1]) = from;
                get<TO_INDEX>(listOfEdges[numOfEdges-1]) = to;
                get<WEIGHT_INDEX>(listOfEdges[numOfEdges-1]) = weight;
                break;
           }
       }
}

void  removeEdge(int from, int to){
        switch ( statement){
            case ADJMATRIX:{
                adjacencyMatrix[from-1][to-1] = 0;
                if (!isOriented) adjacencyMatrix[to-1][from-1] = 0;
                numOfEdges--;
                break;
           }
            case ADJLIST:{
                adjacencyList[from-1].erase(adjacencyList[from-1].find(to));
                if (!isOriented) adjacencyList[to-1].erase(adjacencyList[to-1].find(from));
                numOfEdges--;
                break;
           }
            case LISTOFEDGES:{
                for (auto it = listOfEdges.begin(); it != listOfEdges.end(); it++){
                    if (get<FROM_INDEX>(*it) == from && get<TO_INDEX>(*it) == to){
                        listOfEdges.erase(it);
                        break;
                   }
               }
                numOfEdges--;
                break;
           }
       }
}
    
int  changeEdge(int from, int to, int newWeight){
        int oldWeight;
        switch ( statement){
            case ADJMATRIX:{
                oldWeight = adjacencyMatrix[from-1][to-1];
                adjacencyMatrix[from-1][to-1] = newWeight;
                if (!isOriented) adjacencyMatrix[to-1][from-1] = newWeight;
                break;
           }
            case ADJLIST:{
                oldWeight = adjacencyList[from-1].find(to)->second;
                adjacencyList[from-1].find(to)->second = newWeight;
                if (!isOriented) adjacencyList[to-1].find(from)->second = newWeight;
                break;
           }
            case LISTOFEDGES:{
                for (auto it = listOfEdges.begin(); it != listOfEdges.end(); it++){
                    if (get<FROM_INDEX>(*it) == from && get<TO_INDEX>(*it) == to){
                        oldWeight = get<WEIGHT_INDEX>(*it);
                        get<WEIGHT_INDEX>(*it) = newWeight;
                        break;
                   }
               }
                break;
           }
       }
        return oldWeight;
}
    
    bool checkBipart(vector <char> &graduation){
        queue <int> currentQueue;
        set <int> usingGraph;
        for (int i = 1; i <= numOfEdges; i++) usingGraph.insert(i);
        currentQueue.push(1);
        graduation[1] = 'A';
        int kgraduation = 1;
        transformToadjacencyList();
        while (!currentQueue.empty()){
            int current = currentQueue.front()-1;
            currentQueue.pop();
            for (auto i = adjacencyList[current].begin(); i != adjacencyList[current].end(); i++){
                if (graduation[i->first] != 'A' && graduation[i->first] != 'B'){
                    graduation[i->first] = graduation[current+1] == 'A' ? 'B' : 'A';
                    currentQueue.push(i->first);
                    kgraduation++;
               }
                else{
                    if (graduation[i->first] == graduation[current+1]) return false;
               }
           }
            usingGraph.erase(current+1);
            if (currentQueue.empty() && !usingGraph.empty()) currentQueue.push(*usingGraph.begin());
       }
        return true;
   }
    bool bipartDFS(int j, vector <bool> &usingGraph, vector <int> &currentBipartVector, vector <bool> &checked){
        if (usingGraph[j]) return false;
        usingGraph[j] = true;
        for (auto i = adjacencyList[j-1].begin(); i != adjacencyList[j-1].end(); i++){
            int to = i->first;
            if ((currentBipartVector[to] == -1) || bipartDFS(currentBipartVector[to], usingGraph, currentBipartVector, checked)){
                currentBipartVector[to] = j;
                return true;
           }
       }
        return false;
   }
    vector <pair <int, int> > getMaximumMatchingBipart(){
        vector <int> currentBipartVector(numOfEdges+1, -1);
        vector <pair <int, int> > result;
        vector <char> graduation(numOfEdges+1, 'N');
        vector <bool> checked(numOfEdges+1, false);
        vector <bool> usingGraph(numOfEdges+1, false);
        transformToadjacencyList();
        for (int i = 1; i <= numOfEdges; i++){
            if (graduation[i] == 'B') continue;
            for (auto j = adjacencyList[i-1].begin(); j != adjacencyList[i-1].end(); j++){
                if (currentBipartVector[j->first] == -1){
                    currentBipartVector[j->first] = i;
                    checked[i] = true;
                    break;
               }
           }
       }
        if (checkBipart(graduation)){
            for (int j = 1; j <= numOfEdges; j++){
                if (checked[j] || graduation[j] == 'B'){
                    usingGraph.assign(numOfEdges+1, false);
                    bipartDFS(j, usingGraph, currentBipartVector, checked);
               }
           }
       }
        for (int i = 1; i <= numOfEdges; i++){
            if (currentBipartVector[i] != -1 && currentBipartVector[currentBipartVector[i]] == i && i < currentBipartVector[i]) result.push_back(make_pair(i, currentBipartVector[i]));
       }
        return result;
   }
    
void  transformToadjacencyMatrix(){
        adjacencyMatrix.resize(countOfList);
        for (int i = 0; i < countOfList; i++) adjacencyMatrix[i].resize(countOfList);
        switch ( statement){
            case ADJLIST:{
                for (int i = 0; i < countOfList; i++){
                    for (auto it = adjacencyList[i].begin(); it != adjacencyList[i].end(); it++) adjacencyMatrix[i][it->first] = it->second;
               }
                adjacencyList.clear();
                adjacencyList.shrink_to_fit();
                break;
           }
            case LISTOFEDGES:{
                for (int i = 0; i < numOfEdges; i++){
                    adjacencyMatrix[get<FROM_INDEX>(listOfEdges[i])-1][get<TO_INDEX>(listOfEdges[i])-1] = get<WEIGHT_INDEX>(listOfEdges[i]);
                    if (!isOriented) adjacencyMatrix[get<TO_INDEX>(listOfEdges[i])-1][get<WEIGHT_INDEX>(listOfEdges[i])-1] = get<WEIGHT_INDEX>(listOfEdges[i]);
               }
                listOfEdges.clear();
                listOfEdges.shrink_to_fit();
                break;
           }
       }
         statement = ADJMATRIX;
}

void  transformToadjacencyList(){
        adjacencyList.resize(countOfList);
        switch ( statement){
            case ADJMATRIX:{
                for (int i = 0; i < countOfList; i++){
                    for (int j = 0; j < countOfList; j++){
                        if (adjacencyMatrix[i][j] != 0){
                            adjacencyList[i][j+1] = adjacencyMatrix[i][j];
                            if (!isOriented) adjacencyList[j][i+1] = adjacencyMatrix[i][j];
                       }
                   }
               }
                adjacencyMatrix.clear();
                adjacencyMatrix.shrink_to_fit();
                break;
           }
            case LISTOFEDGES:{
                for (int i = 0; i < numOfEdges; i++){
                    int from = get<FROM_INDEX>(listOfEdges[i]);
                    int to = get<TO_INDEX>(listOfEdges[i]);
                    int weight = get<WEIGHT_INDEX>(listOfEdges[i]);
                    adjacencyList[from-1][to] = weight;
                    if (!isOriented) adjacencyList[to-1][from] = weight;
               }
                listOfEdges.clear();
                listOfEdges.shrink_to_fit();
                break;
           }
       }
         statement = ADJLIST;
        
}

void  transformToListOfEdges(){
        listOfEdges.resize(numOfEdges);
        switch ( statement){
            case ADJMATRIX:{
                int i = 0;
                for (int j = 0; j < countOfList; j++){
                    for (int k = 0; k < countOfList; k++){
                        if (adjacencyMatrix[j][k] != 0){
                            if ((!isOriented && j < k) || isOriented){
                                get<FROM_INDEX>(listOfEdges[i]) = j+1;
                                get<TO_INDEX>(listOfEdges[i]) = k+1;
                                get<WEIGHT_INDEX>(listOfEdges[i]) = adjacencyMatrix[j][k];
                                i++;
                           }
                       }
                   }
               }
                adjacencyMatrix.clear();
                adjacencyMatrix.shrink_to_fit();
                break;
           }
            case ADJLIST:{
                int i = 0;
                for (int j = 0; j < countOfList; j++){
                    for (auto it = adjacencyList[j].begin(); it != adjacencyList[j].end(); it++){
                        get<FROM_INDEX>(listOfEdges[i]) = j+1;
                        get<TO_INDEX>(listOfEdges[i]) = it->first;
                        get<WEIGHT_INDEX>(listOfEdges[i]) = it->second;
                        i++;
                   }
               }
                adjacencyList.clear();
                adjacencyList.shrink_to_fit();
                break;
           }
       }
         statement = LISTOFEDGES;
}
    

int checkEuler(bool &isReal){
        DSU dsu(countOfList);
        bool dsuValue = false;
        int result = 1, count = 0;
        vector <int> scale(countOfList+1, 0);
         transformToListOfEdges();
        for (int i = 0; i < numOfEdges; i++){
            scale[get<FROM_INDEX>( listOfEdges[i])]++;
            scale[get<TO_INDEX>( listOfEdges[i])]++;
       }
        for (int i = 1; i <= countOfList; i++){
            if (scale[i] % 2 == 1){
                count++;
                result = i;
           }
       }
        for (int i = 0; i < numOfEdges; i++){
            int from = get<FROM_INDEX>( listOfEdges[i]);
            int to = get<TO_INDEX>( listOfEdges[i]);
            if (dsu.find(from) != dsu.find(to)){
                dsu.unite(from, to);
           }
       }
        dsuValue = dsu.check();
        (count == 0)?isReal = true : isReal = false;
        if ((count==0 || count==2) && dsuValue) return result;
        else return 0;
}
    
bool checkBridge(int from, int to){
        Graph graph;
        graph.setGraph(ADJLIST, isOriented, weighted, countOfList, numOfEdges);
        DSU dsu(countOfList);
         transformToadjacencyList();
        for (int i = 0; i < countOfList; i++) graph.adjacencyList[i] =  adjacencyList[i];
        graph.removeEdge(from, to);
        for (int i = 0; i < countOfList; i++){
            int a = i+1;
            for (auto j = graph.adjacencyList[i].begin(); j != graph.adjacencyList[i].end(); j++){
                int b = j->first;
                if (dsu.find(a) != dsu.find(b)) dsu.unite(a, b);
           }
       }
        return !dsu.check();
}
    
vector <int> getEuleranTourFleri(){
        Graph graph;
        graph.setGraph(LISTOFEDGES, isOriented, weighted, countOfList, numOfEdges);
        vector <int> result(numOfEdges+1);
        vector <int> scale(countOfList+1, 0);
         transformToListOfEdges();
        for (int i = 0; i < numOfEdges; i++){
            scale[get<FROM_INDEX>( listOfEdges[i])]++;
            scale[get<TO_INDEX>( listOfEdges[i])]++;
            get<FROM_INDEX>(graph.listOfEdges[i]) = get<FROM_INDEX>( listOfEdges[i]);
            get<TO_INDEX>(graph.listOfEdges[i]) = get<TO_INDEX>( listOfEdges[i]);
            get<WEIGHT_INDEX>(graph.listOfEdges[i]) = get<WEIGHT_INDEX>( listOfEdges[i]);
       }
        bool roundedFlag;
        int start = checkEuler(roundedFlag);
        result[0] = start;
         transformToadjacencyList();
        graph.transformToadjacencyList();
        int current = start;
        for (int i = 0; i < numOfEdges; i++){
            bool stepFlag = false;
            for (auto j = graph.adjacencyList[current-1].begin(); j != graph.adjacencyList[current-1].end(); j++){
                int next = j->first;
                if (!graph.checkBridge(current, next) || scale[current] == 1){
                    stepFlag = true;
                    scale[current]--;
                    graph.removeEdge(current, next);
                    current = next;
                    result[i+1] = current;
                    scale[next]--;
                    break;
               }
           }
            if (!stepFlag){
                for (auto j = graph.adjacencyList[current-1].begin(); j != graph.adjacencyList[current-1].end(); j++){
                    int next = j->first;
                    if (!graph.checkBridge(current, next) || scale[next] != 1){
                        scale[current]--;
                        graph.removeEdge(current, next);
                        current = next;
                        result[i+1] = current;
                        scale[next]--;
                        break;
                   }
               }
           }
            stepFlag = false;
       }
        return result;
}
    
vector <int> getEuleranTourEffective(){
        Graph graph;
        graph.setGraph(LISTOFEDGES, isOriented, weighted, countOfList, numOfEdges);
        stack <int> stackPart;
        vector <int> result(numOfEdges+1);
        vector <int> scale(countOfList+1, 0);
         transformToListOfEdges();
        for (int i = 0; i < numOfEdges; i++){
            scale[get<FROM_INDEX>( listOfEdges[i])]++;
            scale[get<TO_INDEX>( listOfEdges[i])]++;
            get<FROM_INDEX>(graph.listOfEdges[i]) = get<FROM_INDEX>( listOfEdges[i]);
            get<TO_INDEX>(graph.listOfEdges[i]) = get<TO_INDEX>( listOfEdges[i]);
            get<WEIGHT_INDEX>(graph.listOfEdges[i]) = get<WEIGHT_INDEX>( listOfEdges[i]);
       }
        bool roundedFlag;
        int start = checkEuler(roundedFlag);
        result[numOfEdges] = start;
        int itans = 0;
         transformToadjacencyList();
        graph.transformToadjacencyList();
        int current = start;
        for (int i = 0; i <= numOfEdges; i++){
            for (auto j = graph.adjacencyList[current-1].begin(); j != graph.adjacencyList[current-1].end(); j++){
                int next = j->first;
                stackPart.push(next);
                scale[current]--;
                graph.removeEdge(current, next);
                current = next;
                scale[next]--;
                break;
           }
            if (graph.adjacencyList[current-1].size() == 0 && !stackPart.empty()){
                while (graph.adjacencyList[stackPart.top()-1].size() == 0){
                    result[itans] = stackPart.top();
                    stackPart.pop();
                    itans++;
                    if (stackPart.empty()) break;
                    current = stackPart.top();
               }
           }
       }
        while (!stackPart.empty()){
            result[itans] = stackPart.top();
            stackPart.pop();
            itans++;
       }
        return result;
}
    
static bool sortWithTuples(const tuple <int,int,int> &ft, const tuple <int,int,int> &st) {
        if (get<WEIGHT_INDEX>(ft)!=get<WEIGHT_INDEX>(st)) return (get<WEIGHT_INDEX>(ft)<get<WEIGHT_INDEX>(st));
        else {
            if (get<FROM_INDEX>(ft)!=get<FROM_INDEX>(st)) return (get<FROM_INDEX>(ft)<get<FROM_INDEX>(st));
            else return (get<TO_INDEX>(ft)<get<TO_INDEX>(st));
        }
}
    
Graph getSpanningTreeKruskal()
    {
        Graph graph;
        
        graph.setGraph(LISTOFEDGES, isOriented, weighted, countOfList, 0);
        
        DSU dsu(countOfList);
        
        transformToListOfEdges();
        
        sort(listOfEdges.begin(), listOfEdges.end(), sortWithTuples);
        
        for (int i = 0; i < countOfList; i++) {
            int weight, from, to;
            weight = get<WEIGHT_INDEX>(listOfEdges[i]);
            from = get<FROM_INDEX>(listOfEdges[i]);
            to = get<TO_INDEX>(listOfEdges[i]);
            if (dsu.find(to)!=dsu.find(from)) {
                graph.addEdge(from,to,weight);
                dsu.unite(from,to);
            }
        }
        return graph;
    }
    
};

#endif /* Graph_h */
